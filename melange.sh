#!/bin/bash

to_find="/var/www/html/mutom/data/motsATrouve.txt"
public_mots="/var/www/html/mutom/public/mots"
deja_trouv="/home/ubuntu/mutom/deja_trouve.txt"
#cat $to_find
#ls $public_mots
# Script à éxécuter tous les 28 jours à 23h59
# 59 23  28 * * bash /home/ubuntu/melange.sh

#### Génère les 28 prochains mots dans le mutom
for i in {0..29}; do
  len_to_find=$(cat $to_find | wc -l )
  len_deja_trouv=$(cat $deja_trouv | wc -l)
  word="$(shuf -n 1 $to_find)"
  if [[ $len_deja_trouv -ge $len_to_find ]]
  then
    echo "Tous les mots ont étaient trouvés"
    exit 1
  fi
  while grep -Fxq "$word" $deja_trouv
  do
    word=$(shuf -n 1 $to_find)
#    echo $word
  done
  echo "$word" >> $deja_trouv
  date=$(date '+%Y-%m-%d' -d "+$i day")
  hash=$(echo -n "34ccc522-c264-4e51-b293-fd5bd60ef7aa-$date" | base64)
  echo -n ${word^^} > $public_mots/$hash.txt

done